package com.cerotid.bank.model;

public enum CheckType {
	PAPERCHECK, ECHECK;
}
