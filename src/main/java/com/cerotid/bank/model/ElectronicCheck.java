package com.cerotid.bank.model;

public class ElectronicCheck extends Transaction {
	private CheckType checkType;
	
	public ElectronicCheck(double ammount, double fee, String receiverFirstName, String receiverLastName) {
		super(ammount, fee, receiverFirstName, receiverLastName);
		// TODO Auto-generated constructor stub
	}

	public ElectronicCheck() {
		// TODO Auto-generated constructor stub
	}

	public CheckType getCheckType() {
		return checkType;
	}

	public void setCheckType(CheckType checkType) {
		this.checkType = checkType;
	}

	@Override
	public String toString() {
		return "ElectronicCheck [checkType=" + checkType + "]";
	}

}
