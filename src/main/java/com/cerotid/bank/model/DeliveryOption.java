package com.cerotid.bank.model;

public enum DeliveryOption {
	TENMINUTE, TWENTYFOURHRS;
	private String time;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}
