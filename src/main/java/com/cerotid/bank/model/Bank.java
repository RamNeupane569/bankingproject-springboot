package com.cerotid.bank.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Bank implements Serializable {
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -925035125705577504L;
	public final String bankName = "ANC bank";
	// private ArrayList<Customer> customers;
	ArrayList<Customer> customers = new ArrayList<Customer>();

	public Bank() {
		this.customers = new ArrayList<>();
	}

	public void addCustomer(Customer customer) {
		customers.add(customer);
	}
	
	public List<Customer> getAllCustomers(){
		return customers;
	}

	public String printBankName(String bankName) {
		System.out.println("Welcome to: " + bankName);
		return bankName;
	}

	public String printBankDetails() {
		System.out.println("Bank details: " + this.bankName + this.customers);
		return bankName;
	}

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}

	public String getBankName() {
		return bankName;
	}

	@Override
	public String toString() {
		return "Bank [bankName=" + bankName + ", customers=" + customers + "]";

	}

	public Customer getCustomer(String ssn) {
		// TODO Auto-generated method stub
		for (Customer customer : customers) {
			if (ssn.equals(customer.getSsn())) {
				return customer;
			}
		}
		return null;
	}

	public static Bank getInstance() {
		return null; // give bank object in singleton pattern way
	}

}
