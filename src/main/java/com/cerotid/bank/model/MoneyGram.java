package com.cerotid.bank.model;

public class MoneyGram extends Transaction {

	private DeliveryOption deliveryOption;
	private String destinationCountry;


	public MoneyGram(double amount, double fee, String receiverFirstName, String receiverLastName) {
		super(amount, fee, receiverFirstName, receiverLastName);
		// TODO Auto-generated constructor stub
	}

	public MoneyGram(double amount, double fee, String receiverFirstName, String receiverLastName,
			DeliveryOption deliveryOption, String destinationCountry) {
		super(amount, fee, receiverFirstName, receiverLastName);
		this.deliveryOption = deliveryOption;
		this.destinationCountry = destinationCountry;
	}

	public MoneyGram() {
		// TODO Auto-generated constructor stub
	}

	public DeliveryOption getDeliveryOption() {
		return deliveryOption;
	}

	public void setDeliveryOption(DeliveryOption deliveryOption) {
		this.deliveryOption = deliveryOption;
	}

	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

	@Override
	public String toString() {
		return "MoneyGram [deliveryOption=" + deliveryOption + ", destinationCountry=" + destinationCountry + "]";
	}

}
