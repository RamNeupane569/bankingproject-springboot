package com.cerotid.bank.model;

public class WireTransfer extends Transaction {

	String beneficiaryfirstName;
	String beneficiarylastName;
	String intermediaryBankSWIFTCode;
	String beneficiaryBankName;
	String beneficiaryAccountNumber;

	public WireTransfer(double ammount, double fee, String receiverFirstName, String receiverLastName,
			String beneficiaryfirstName, String beneficiarylastName, String intermediaryBankSWIFTCode,
			String beneficiaryBankName, String beneficiaryAccountNumber) {
		super(ammount, fee, receiverFirstName, receiverLastName);

		this.beneficiaryfirstName = beneficiaryfirstName;
		this.beneficiarylastName = beneficiarylastName;
		this.intermediaryBankSWIFTCode = intermediaryBankSWIFTCode;
		this.beneficiaryBankName = beneficiaryBankName;
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}

	public WireTransfer(double ammount, double fee, String receiverFirstName, String receiverLastName) {
		super(ammount, fee, receiverFirstName, receiverLastName);
		// TODO Auto-generated constructor stub
	}

	public WireTransfer() {
		// TODO Auto-generated constructor stub
	}

	public String getReceiverFirstName() {
		return beneficiaryfirstName;
	}

	public void setReceiverFirstName(String firstName) {
		this.beneficiaryfirstName = firstName;
	}

	public String getReceiverLastName() {
		return beneficiarylastName;
	}

	public void setReceiverLastName(String lastName) {
		this.beneficiarylastName = lastName;
	}

	public String getIntermediaryBankSWIFTCode() {
		return intermediaryBankSWIFTCode;
	}

	public void setIntermediaryBankSWIFTCode(String intermediaryBankSWIFTCode) {
		this.intermediaryBankSWIFTCode = intermediaryBankSWIFTCode;
	}

	public String getBeneficiaryBankName() {
		return beneficiaryBankName;
	}

	public void setBeneficiaryBankName(String beneficiaryBankName) {
		this.beneficiaryBankName = beneficiaryBankName;
	}

	public String getBeneficiaryAccountNumber() {
		return beneficiaryAccountNumber;
	}

	public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
		this.beneficiaryAccountNumber = beneficiaryAccountNumber;
	}

	@Override
	public String toString() {
		return "WireTransfer [beneficiaryfirstName=" + beneficiaryfirstName + ", beneficiarylastName="
				+ beneficiarylastName + ", intermediaryBankSWIFTCode=" + intermediaryBankSWIFTCode
				+ ", beneficiaryBankName=" + beneficiaryBankName + ", beneficiaryAccountNumber="
				+ beneficiaryAccountNumber + "]";
	}

}
