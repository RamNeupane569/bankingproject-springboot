package com.cerotid.bank.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ZipCodeValidator {
	public static boolean isValid(String zipcode) {

		String regex = "^[0-9]{5}(?:-[0-9]{4})?$";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(zipcode);
		if (matcher.matches()) {
			return true;
		} else
			return false;
	}
}
