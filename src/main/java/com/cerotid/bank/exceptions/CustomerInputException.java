package com.cerotid.bank.exceptions;

public class CustomerInputException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -936386910163041863L;
	
	public CustomerInputException(String message) {
		super(message);
	}
	
	public CustomerInputException(String message, Throwable throwable ) {
		super(message, throwable);
	}

	public CustomerInputException(Throwable throwable ) {
		super(throwable);
	}
}
