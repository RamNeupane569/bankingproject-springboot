package com.cerotid.bo;

/*
 For each menu above create a Interface "BankBO" under com.cerotid.bo package. Add following contract:
	public void addCustomer(Customer customer);
	public boolean openAccount(Customer customer, Account account);
	public void sendMoney(Customer customer, Transaction transaction);
	public void depositMoneyInCustomerAccount(Customer customer);
	public void editCustomerInfo(Customer customer);
	public Customer getCustomerInfo(String ssn);
	public void printBankStatus();
	public void serializeBank();
	public List<Customer> getCustomersByState(String StateCode);
 */
import java.util.List;

import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.Transaction;

public interface BankBO {
	public void addCustomer(Customer customer); //completed

	public boolean openAccount(Customer customer, Account account);

	public void sendMoney(Customer customer, Account account, Transaction transaction);

	public void editCustomerInfo(Customer customer);//next goal

	public Customer getCustomerInfo(String ssn); //next goal //completed

	public void printBankStatus(); //completed

	public void serializeBank(); //completed

	public void deserializeBank(); //completed

	public List<Customer> getCustomersByState(String stateCode);// next goal

	

	public List<Customer> getCustomers();
	

}
