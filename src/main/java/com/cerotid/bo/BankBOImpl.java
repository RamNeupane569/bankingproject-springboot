package com.cerotid.bo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
/*
 Create a concrete class "BankBOImpl" implementing BankBO
	Provide implementation for every method overridden in concrete class
 */
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.Bank;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.ElectronicCheck;
import com.cerotid.bank.model.MoneyGram;
import com.cerotid.bank.model.Transaction;
import com.cerotid.bank.model.WireTransfer;

@Service
public class BankBOImpl implements BankBO, Serializable {

	/**
	 * 
	 */

	private static final long serialVersionUID = -1794222018400230614L;
	

	private static Bank bank;

	static {
		bank = new Bank();
	}

	public BankBOImpl() {
		// this.bank = Bank.getInstance();
		// call deserialize here

		//deserializeBank();
	}

	@Override
	public void addCustomer(Customer customer) {
		bank.getCustomers().add(customer);
	}

	@Override
	public boolean openAccount(Customer customer, Account account) {
		if (bank.getCustomers().contains(customer)) {
			int i = bank.getCustomers().indexOf(customer);
			bank.getCustomers().get(i).addAccount(account);
			return true;
		} else {
			System.out.println("Customer do not exist!");
			return false;
		}

	}

	@Override
	public void sendMoney(Customer customer, Account account, Transaction transaction) {
		System.out.println("Money Sent");

		if (transaction instanceof ElectronicCheck) {

		} else if (transaction instanceof WireTransfer) {

		} else if (transaction instanceof MoneyGram) {

		}

		// Business logic goes here
		printTransactionInfo(customer, account, transaction);
	}

	private void printTransactionInfo(Customer customer, Account account, Transaction transaction) {
		System.out.println(customer.getFirstName() + " Sent " + transaction.getAmount() +" "+ "as transaction Type"
			+ " "	+ transaction.getClass().getSimpleName());

	}

	@Override
	public void editCustomerInfo(Customer customer) {
		bank.getCustomers();
	}

	@Override
	public Customer getCustomerInfo(String ssn) {
		// ArrayList<Customer> customerList = bank.getCustomers();
		return bank.getCustomer(ssn);
	}

	@Override
	public void printBankStatus() {
		System.out.println(bank);

	}

	@Override
	public void serializeBank() {
		// ArrayList<Customer> data = bank.getCustomers(); wrong object, technical debt
		// serialize bank only
		try {
			FileOutputStream fos = new FileOutputStream("data.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);

			oos.writeObject(bank);
			System.out.println(bank);

			fos.close();
			oos.close();
			System.out.println("Serialized data is saved");
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	@Override
	public void deserializeBank() {
		try {
			FileInputStream fis = new FileInputStream("data.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);

			bank = (Bank) ois.readObject();
			fis.close();
			ois.close();

		} catch (FileNotFoundException e) {
			System.out.println("Data.ser fileis not found");

			e.printStackTrace();
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	@Override
	public List<Customer> getCustomersByState(String stateCode) {

		ArrayList<Customer> customerByState = new ArrayList<>();
		ArrayList<Customer> customerList = bank.getCustomers();
		
		for (Customer customer : customerList) {
			if (stateCode.equalsIgnoreCase(customer.getAddress().getStateCode())) {
				customerByState.add(customer);
			}
		}
		return customerByState;
	}

	@Override
	public String toString() {
		return "BankBOImpl [bank=" + bank + "]";
	}

	@Override
	public List<Customer> getCustomers() {
		return this.bank.getAllCustomers();
		
		
	}



}
