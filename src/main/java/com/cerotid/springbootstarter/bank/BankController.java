package com.cerotid.springbootstarter.bank;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cerotid.bank.model.Customer;
import com.cerotid.bo.BankBO;

@RestController
public class BankController {
	
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private BankBO bankBO;

	@RequestMapping("/customers")
	public List<Customer> getCustomers() {
		log.info("Retrieving customers from the bank");
		return bankBO.getCustomers();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/customers")
	public void addCustomer(@RequestBody Customer customer) {
		bankBO.addCustomer(customer);
	}

}
