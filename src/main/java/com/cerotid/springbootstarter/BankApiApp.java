package com.cerotid.springbootstarter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.cerotid.bank", "com.cerotid.bank.exceptions", "com.cerotid.bank.model",
		"com.cerotid.bank.validator", "com.cerotid.springbootstarter", "com.cerotid.bo",
		"com.cerotid.springbootstarter.bank" })

public class BankApiApp {
	static Logger logger = LoggerFactory.getLogger(BankApiApp.class);

	public static void main(String[] args) {
		logger.info("this is a info message");
		logger.warn("this is a warn message");
		logger.error("this is a error message");

		SpringApplication.run(BankApiApp.class, args);
	}

}
